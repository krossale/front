import React, { useState } from 'react';
import { BrowserRouter as Router, Routes, Route, Navigate, useLocation } from 'react-router-dom';
import Header from "./components/header/Header.jsx";
import Navigation from "./components/navigation/Navigation.js";
import Footer from "./components/footer/Footer.js";
import HomePage from "./HomePage";
import Page1 from "./Page1";
import MainComponent from "./MainComponent.js";
import RegisterPage from "./RegisterPage.jsx";
import Karty from "./Karty.js";
import Mzda from "./Mzda.js";
import Investice from "./Investice.js";
import CategoriesPage from "./CategoriesPage.jsx";
import WalletPage from "./WalletPage.jsx";
import TransactionsPage from "./TransactionsPage.jsx";

const Layout = () => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const location = useLocation();

  const showHeaderAndNav = location.pathname !== '/registration';

  const requireAuth = (element) => {
    return isAuthenticated ? element : <Navigate to="/registration" />;
  };
  return (
    <>
      {showHeaderAndNav && <Header />}
      {showHeaderAndNav && <Navigation />}
      { <Routes>
          <Route exact path="/registration" element={<RegisterPage />} />
          <Route exact path="/categories" element={(<CategoriesPage />)}  />
          <Route exact path="/wallet" element={<WalletPage />} />
          <Route exact path="/transactions" element={<TransactionsPage />} />
          <Route exact path="/goals" element={<Investice />} />
        </Routes>}
      {showHeaderAndNav && <Footer />}
    </>
  );
};

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route path="/*" element={<Layout />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
