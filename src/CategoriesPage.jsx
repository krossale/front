import React, { useState, useEffect } from 'react';
import { Container, TextField, Button, Typography, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@mui/material';
import './CategoriesPage.css';

const CategoriesPage = () => {
  const [categories, setCategories] = useState([]);
  const [newCategoryName, setNewCategoryName] = useState('');

  useEffect(() => {
    fetchCategories();
  }, []);

  const handleDeleteCategory = async (categoryId) => {
    try {
      const email = localStorage.getItem('email');
      const password = localStorage.getItem('password');
    
      const encodedCredentials = btoa(email + ':' + password);

      const response = await fetch(`http://localhost:8080/rest/categories/${categoryId}`, {
        method: 'DELETE',
        headers: {
          'Authorization': `Basic ${encodedCredentials}`,
          'Content-Type': 'application/json',
          'Accept': '*/*',
        },
      });

      if (response.ok) {
        await fetchCategories();
      } else {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
    } catch (error) {
      console.error('There was a problem with deleting the category:', error);
    }
  };

  const fetchCategories = async () => {
    try {
      const email = localStorage.getItem('email');
      const password = localStorage.getItem('password');
    
      const encodedCredentials = btoa(email + ':' + password);

      const response = await fetch('http://localhost:8080/rest/categories/getAll', {
        method: 'GET',
        headers: {
          'Authorization': `Basic ${encodedCredentials}`,
          'Content-Type': 'application/json',
          'Accept': '*/*',
        },
      });
  
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
  
      const categoriesData = await response.json();
      setCategories(categoriesData);
    } catch (error) {
      console.error('There was a problem with fetching categories:', error);
    }
  };


  const handleAddCategory = async () => {
    const email = localStorage.getItem('email');
    const password = localStorage.getItem('password');
  
    const encodedCredentials = btoa(email + ':' + password);

    const categoryData = {
      name: newCategoryName
    };
    try {
      const response = await fetch('http://localhost:8080/rest/categories', {
        method: 'POST',
        headers: {
          'Authorization': `Basic ${encodedCredentials}`,
          'Content-Type': 'application/json',
          'Accept': '*/*',
        },
        body: JSON.stringify(categoryData),
      });

      if (response.ok) {
        await fetchCategories();
        setNewCategoryName('');
      } else {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
    } catch (error) {
      console.error('There was a problem with adding a category:', error);
    }
  };

  return (
    <div className="main-layout">
        <Container maxWidth="md">
      <Typography variant="h4" style={{ margin: '20px 0', textAlign: 'center' }}>Categories</Typography>
      <div style={{ display: 'flex', justifyContent: 'space-between', marginBottom: '20px', alignItems: 'end' }}>
        <TextField
          label="Category Name"
          value={newCategoryName}
          onChange={(e) => setNewCategoryName(e.target.value)}
          margin="normal"
          className='textField'
        />
        <Button variant="contained" color="primary" onClick={handleAddCategory} className='buttonField'>
          Add
        </Button>
      </div>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Category Name</TableCell>
              <TableCell>Actions</TableCell> {/* Добавлен столбец для кнопки удаления */}
            </TableRow>
          </TableHead>
          <TableBody>
            {categories.map((category) => (
              <TableRow key={category.category_id}>
                <TableCell component="th" scope="row">
                  {category.name}
                </TableCell>
                <TableCell>
                  <Button 
                    variant="contained" 
                    color="secondary" 
                    onClick={() => handleDeleteCategory(category.categoryId)}
                  >
                    Delete
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
    </div>
    
  );
};

export default CategoriesPage;
