import React from "react";
import "./HomePage.css";
import RightBar from "./components/rightbar/RightBar.js";

const HomePage = () => {
  return (
    <div class="page-all">
      <div className="home">
        <h2>Nová platba</h2>
        <div class="ucet">
          Z účtu:
          <div>Běžný osobní účet Main_Family</div>
        </div>
        <div>
          <div>Částka</div>
          <input type="text" placeholder="CZK" />
        </div>
        <div>
          <div> Datum splatnosti</div>
          <input type="date" />
        </div>
        <div>
          <div>Category</div>

          <input type="text" />
        </div>
        <div>
          <div> Zpráva pro mne</div>
          <input type="text" />
        </div>
        <button class="button">Podtvrdit</button>
      </div>
      <RightBar></RightBar>
    </div>
  );
};

export default HomePage;
