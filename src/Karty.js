import React, { useState } from "react";
import Sidebar from "./components/sidebar/SideBar.js";
import HomePage from "./HomePage";
import "./MainComponent.css";
import MojeKarty from "./MojeKarty.js";

const MainComponent = () => {
  const [activeComponent, setActiveComponent] = useState(<HomePage />);

  const items = [{ title: "Moje Karty", component: <MojeKarty /> }];

  return (
    <div className="main-layout">
      <Sidebar items={items} setActiveComponent={setActiveComponent} />
    </div>
  );
};

export default MainComponent;
