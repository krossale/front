import React from "react";
import "./HomePage.css";
import RightBar from "./components/rightbar/RightBar.js";

const Konverze = () => {
  return (
    <div class="page-all">
      <div className="home">
        <h2>Konverze</h2>
        <p>Z účtu CZK</p>
        <div class="ucet">Na účet EUR</div>
        <div>
          <div>Částka</div>
          <input type="text" placeholder="CZK" />
        </div>

        <div>
          <div>Bude</div>
          <input type="text" placeholder="EUR" />
        </div>

        <div>
          <div> Zpráva pro mne</div>
          <input type="text" />
        </div>
        <button class="button">Podtvrdit</button>
      </div>
      <RightBar></RightBar>
    </div>
  );
};

export default Konverze;
