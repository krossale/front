import React, { useState } from "react";
import Sidebar from "./components/sidebar/SideBar.js";
import HomePage from "./HomePage";
import Konverze from "./Konverze.js";
import "./MainComponent.css";
import Page1 from "./Page1.js";
import Taxes from "./Taxes.js";

const MainComponent = () => {
  const [activeComponent, setActiveComponent] = useState(<HomePage />);

  const items = [
    { title: "Nová platba", component: <HomePage /> },
    { title: "Převod mezi vlastními účty", component: <Konverze /> },
    { title: "Taxes", component: <Taxes /> },
    // Добавьте здесь другие компоненты, которые хотите переключать
  ];

  return (
    <div className="main-layout">
      <Sidebar items={items} setActiveComponent={setActiveComponent} />
    </div>
  );
};

export default MainComponent;
