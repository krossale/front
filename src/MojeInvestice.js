import React from "react";
import "./HomePage.css";
import RightBar from "./components/rightbar/RightBar.js";
import MojeInvesticeComponent from "./components/mojeInvestice/MojeInvesticeComponent.js";

const MojeInvestice = () => {
  return (
    <div class="page-all">
      <div className="home">
        <h2>Moje Investice</h2>
        <MojeInvesticeComponent></MojeInvesticeComponent>
        <MojeInvesticeComponent></MojeInvesticeComponent>

        <MojeInvesticeComponent></MojeInvesticeComponent>

        <MojeInvesticeComponent></MojeInvesticeComponent>
      </div>
      <RightBar></RightBar>
    </div>
  );
};

export default MojeInvestice;
