import React from "react";
import "./HomePage.css";
import RightBar from "./components/rightbar/RightBar.js";
import MojeCartyComponent from "./components/mojekarty/MojeCartyComponent.js";

const MojeKarty = () => {
  return (
    <div class="page-all">
      <div className="home">
        <h2>Moje karty</h2>
        <MojeCartyComponent></MojeCartyComponent>
        <MojeCartyComponent></MojeCartyComponent>

        <MojeCartyComponent></MojeCartyComponent>

        <MojeCartyComponent></MojeCartyComponent>
      </div>
      <RightBar></RightBar>
    </div>
  );
};

export default MojeKarty;
