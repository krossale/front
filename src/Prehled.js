import React from "react";
import "./HomePage.css";
import RightBar from "./components/rightbar/RightBar.js";

const Prehled = () => {
  return (
    <div class="page-all">
      <div className="home">
        <h2>Investice: Přehled možnosti</h2>

        <div>
          <div>Nejlepši Nabidky</div>
          List of Nejlepšich nabidek ...
        </div>
        <div>
          <div> Nejpopularnějši</div>
          List of Nejpopularnějšich nabidek ...
        </div>
      </div>
      <RightBar></RightBar>
    </div>
  );
};

export default Prehled;
