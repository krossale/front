import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Container, TextField, Button, Typography } from '@mui/material';
import './RegistrationPage.css';

const RegistrationPage = () => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();

    const userData = {
      username: username,
      email: email,
      password: password
    };

    try {
      // localStorage.setItem('username', username);
      localStorage.setItem('email', userData.email);
      localStorage.setItem('password', userData.password);
      console.log(userData)

      const response = await fetch('http://localhost:8080/rest/user/register', {
        method: 'POST',
         headers: {
          'Authorization': 'Basic YWRtaW5AdGVzdC50ZXN0OmFkbWluMQ==',
          'Cache-Control': 'no-cache',
          'Content-Type': 'application/json',
          'Accept': '*/*',
        },
        body: JSON.stringify(userData),
      });

  
      if (response.ok) {
  
      } else {
        console.error('Registration failed');
      }


    } catch (error) {
      console.error('Error:', error);
    }

    localStorage.setItem('username', userData.username);
    localStorage.setItem('email', userData.email);
    localStorage.setItem('password', userData.password);

    navigate('/wallet');
  };

  return (
    <div className="registration-container">
<Container maxWidth="sm">
      <Typography variant="h4" style={{ textAlign: 'center', margin: '20px 0' }}>Registration</Typography>
      <form onSubmit={handleSubmit} noValidate autoComplete="off">
        <TextField
          fullWidth
          label="Username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
          margin="normal"
        />
        <TextField
          fullWidth
          label="Email"
          type="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          margin="normal"
        />
        <TextField
          fullWidth
          label="Password"
          type="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          margin="normal"
        />
        <Button variant="contained" color="primary" type="submit" style={{ marginTop: '20px' }}>
          Register
        </Button>
      </form>
    </Container>
    </div>
  );
};

export default RegistrationPage;
