import React from "react";
import "./HomePage.css";
import RightBar from "./components/rightbar/RightBar.js";

const Taxes = () => {
  return (
    <div class="page-all">
      <div className="home">
        <h2>Taxes</h2>
        <div class="ucet">
          Z účtu:
          <div>Běžný osobní účet Main_Family</div>
        </div>
        <div>
          <div>Částka</div>
          <input type="text" placeholder="CZK" />
        </div>

        <div>
          <div> Datum splatnosti</div>
          <input type="date" />
        </div>

        <button class="button">Podtvrdit</button>
      </div>
      <RightBar></RightBar>
    </div>
  );
};

export default Taxes;
