import React, { useState, useEffect } from 'react';
import { Container, TextField, Button, Typography, MenuItem, Select, FormControl, InputLabel } from '@mui/material';

const TransactionsPage = () => {
  const [transaction, setTransaction] = useState({
    money: '',
    description: '',
    typeTransaction: '',
    category: {
      name: '',
    }
  });
  const [categories, setCategories] = useState([]);



  useEffect(() => {
    fetchCategories();
  }, []);

  const fetchCategories = async () => {
    try {
      const email = localStorage.getItem('email');
      const password = localStorage.getItem('password');
    
      const encodedCredentials = btoa(email + ':' + password);

      const response = await fetch('http://localhost:8080/rest/categories/getAll', {
        method: 'GET',
        headers: {
          'Authorization': `Basic ${encodedCredentials}`,
          'Content-Type': 'application/json',
          'Accept': '*/*',
        },
      });
  
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
  
      const categoriesData = await response.json();
      setCategories(categoriesData);
    } catch (error) {
      console.error('There was a problem with fetching categories:', error);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
  
    const email = localStorage.getItem('email');
    const password = localStorage.getItem('password');
    const encodedCredentials = btoa(email + ':' + password);
  
    try {
      const response = await fetch('http://localhost:8080/rest/transaction/new', {
        method: 'POST',
        headers: {
          'Authorization': `Basic ${encodedCredentials}`,
          'Content-Type': 'application/json',
          'Accept': '*/*',
        },
        body: JSON.stringify(transaction),
      });
  
      if (response.ok) {
        console.log('Transaction added successfully');
      } else {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
    } catch (error) {
      console.error('There was a problem with adding the transaction:', error);
    }
  };
  const handleInputChange = (e) => {
    if (e.target.name === 'category') {
      const categoryName = categories.find(cat => cat.categoryId === e.target.value)?.name;
      setTransaction({ ...transaction, category: { name: categoryName } });
    } else {
      setTransaction({ ...transaction, [e.target.name]: e.target.value });
    }
  };

  return (
    <Container maxWidth="sm">
      <Typography variant="h4" style={{ margin: '20px 0', textAlign: 'center' }}>Add Transaction</Typography>
      <form onSubmit={handleSubmit} noValidate autoComplete="off">
        <TextField
          fullWidth
          label="Money"
          name="money"
          type="number"
          value={transaction.money}
          onChange={handleInputChange}
          margin="normal"
        />
        <TextField
          fullWidth
          label="Description"
          name="description"
          value={transaction.description}
          onChange={handleInputChange}
          margin="normal"
        />
        <FormControl fullWidth margin="normal">
          <InputLabel>Type</InputLabel>
          <Select
            name="typeTransaction"
            value={transaction.typeTransaction}
            onChange={handleInputChange}
            label="Type"
          >
            <MenuItem value="INCOME">INCOME</MenuItem>
            <MenuItem value="EXPENSE">EXPENSE</MenuItem>
          </Select>
        </FormControl>
        <FormControl fullWidth margin="normal">
          <InputLabel>Category</InputLabel>
          <Select
            name="category"
            value={transaction.wallet}
            onChange={handleInputChange}
            label="Category"
          >
            {categories.map(category => (
              <MenuItem key={category.categoryId} value={category.categoryId}>
                {category.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <Button variant="contained" color="primary" type="submit" style={{ marginTop: '20px' }}>
          Add Transaction
        </Button>
      </form>
    </Container>
  );
};

export default TransactionsPage;
