import React, { useState, useEffect } from 'react';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Typography, Container } from '@mui/material';

const TransactionsListPage = () => {
  const [transactions, setTransactions] = useState([]);

  useEffect(() => {
    fetchTransactions();
  }, []);

  const fetchTransactions = async () => {
    try {
      const email = localStorage.getItem('email');
      const password = localStorage.getItem('password');
      const encodedCredentials = btoa(email + ':' + password);
  
      const response = await fetch('http://localhost:8080/rest/transaction/getAll', {
        method: 'GET',
        headers: {
          'Authorization': `Basic ${encodedCredentials}`,
          'Content-Type': 'application/json',
          'Accept': '*/*',
        },
      });
  
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
  
      const transactionsData = await response.json();
      setTransactions(transactionsData);
    } catch (error) {
      console.error('There was a problem with fetching transactions:', error);
    }
  };
  

  return (
    <Container maxWidth="md">
      <Typography variant="h4" style={{ margin: '20px 0', textAlign: 'center' }}>Transactions</Typography>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              {/* <TableCell>ID</TableCell> */}
              <TableCell>Description</TableCell>
              <TableCell>Money</TableCell>
              <TableCell>type Transaction</TableCell>
              <TableCell>Category</TableCell>
              <TableCell>Wallet</TableCell>
              <TableCell>date Time</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {transactions.map((transaction) => (
              <TableRow key={transaction.transactions_id}>
                {/* <TableCell>{transaction.transactions_id}</TableCell> */}
                <TableCell>{transaction.description}</TableCell>
                <TableCell>{transaction.money}</TableCell>
                <TableCell>{transaction.typeTransaction}</TableCell>
                <TableCell>{transaction.category}</TableCell>
                <TableCell>{transaction.wallet}</TableCell>
                <TableCell>{transaction.dateTime}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
};

export default TransactionsListPage;
