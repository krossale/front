import React, { useState } from "react";
import Sidebar from "./components/sidebar/SideBar.js";
import HomePage from "./HomePage";
import "./MainComponent.css";
import TransactionsCreatePage from "./TransactionsCreatePage.jsx";
import TransactionsListPage from "./TransactionsListPage.jsx";


const MainComponent = () => {
  const [activeComponent, setActiveComponent] = useState(<HomePage />);

  const items = [
    { title: "New Transaction", component: <TransactionsCreatePage /> },
    { title: "All Transactions", component: <TransactionsListPage /> },
];

  return (
    <div className="main-layout">
      <Sidebar items={items} setActiveComponent={setActiveComponent} />
    </div>
  );
};

export default MainComponent;
