import React, { useState } from 'react';
import { TextField, Button, Container, Typography } from '@mui/material';

const WalletCreatePage = () => {
  const [wallet, setWallet] = useState({
    name: '',
    amount: '',
    client: '',
    budgetLimit: '',
    currency: ''
  });

  const handleInputChange = (e) => {
    setWallet({ ...wallet, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    // send to server
    console.log('Creating Wallet:', wallet);
  };

  return (
    <Container maxWidth="sm">
      <Typography variant="h4" style={{ textAlign: 'center', margin: '20px 0' }}>Create new Wallet</Typography>
      <form onSubmit={handleSubmit} noValidate autoComplete="off">
        <TextField
          fullWidth
          label="Wallet Name"
          name="name"
          value={wallet.name}
          onChange={handleInputChange}
          margin="normal"
        />
        <TextField
          fullWidth
          label="Budget Limit"
          name="budgetLimit"
          type="number"
          value={wallet.budgetLimit}
          onChange={handleInputChange}
          margin="normal"
        />
        <TextField
          fullWidth
          label="Currency (CZK, USD, EUR)"
          name="currency"
          value={wallet.currency}
          onChange={handleInputChange}
          margin="normal"
        />
        <Button variant="contained" color="primary" type="submit" style={{ marginTop: '20px' }}>
          Create
        </Button>
      </form>
    </Container>
  );
};

export default WalletCreatePage;
