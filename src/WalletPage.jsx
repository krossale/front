import React, { useState } from "react";
import Sidebar from "./components/sidebar/SideBar.js";
import "./WalletPage.css";
import WalletCreatePage from "./WalletCreatePage.jsx";
import WalletsListPage from "./WalletsListPage.jsx";


const WalletPage = () => {
  const [activeComponent, setActiveComponent] = useState(<WalletCreatePage />);

  const items = [
    // { title: "Create Wallet", component: <WalletCreatePage /> }, 
    { title: "My Wallet", component: <WalletsListPage /> }
];

  return (
    <div className="main-layout">
      <Sidebar items={items} setActiveComponent={setActiveComponent} />
    </div>
  );
};

export default WalletPage;
