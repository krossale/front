import React, { useState, useEffect } from 'react';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, TextField, Button, Select, MenuItem } from '@mui/material';
import './WalletsListPage.css';


const WalletsListPage = () => {
  const [wallets, setWallets] = useState([]);
  const [inputValue, setInputValue] = useState('');


  useEffect(() => {
    fetchWallets();
  }, []);

  const handleInputChange = (event) => {
    setInputValue(event.target.value);
  };

  const handleAddMoney = async () => {
    const email = localStorage.getItem('email');
    const password = localStorage.getItem('password');
    const encodedCredentials = btoa(email + ':' + password);

    try {
      const response = await fetch(`http://localhost:8080/rest/wallet/money`, {
        method: 'PUT',
        headers: {
          'Authorization': `Basic ${encodedCredentials}`,
          'Content-Type': 'application/json',
          'Accept': '*/*',
        },
        body:  Number(inputValue) + ""
      });
      window.location.reload();
    } catch (error) {
      console.error('There was a problem:', error);
    }
  };

  const handleCurrencyChange = async (currency) => {
    const email = localStorage.getItem('email');
    const password = localStorage.getItem('password');
    const encodedCredentials = btoa(email + ':' + password);

    try {
      const response = await fetch(`http://localhost:8080/rest/wallet/currency?currency=${currency}`, {
        method: 'PUT',
        headers: {
          'Authorization': `Basic ${encodedCredentials}`,
          'Content-Type': 'application/json',
          'Accept': '*/*',
        },
      });
      window.location.reload();
    } catch (error) {
      console.error('There was a problem:', error);
    }
  };

  const fetchWallets = async () => {
    try {
      const email = localStorage.getItem('email');
      const password = localStorage.getItem('password');

      const userData = {
        email: email,
        password: password
      };

      const encodedCredentials = btoa(email + ':' + password);

      console.log(123)

      const response = await fetch('http://localhost:8080/rest/wallet/myWallet', {
        method: 'GET',
         headers: {
          'Authorization': `Basic ${encodedCredentials}`,
          'Cache-Control': 'no-cache',
          'Content-Type': 'application/json',
          'Accept': '*/*',
        },
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const walletData = await response.json();
      setWallets([walletData]);
    } catch (error) {
      console.error('There was a problem with your fetch operation:', error);
    }
  };
  useEffect(() => {
    fetchWallets();
  }, []);

  return (
      <div>
      <TableContainer component={Paper}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>clientEmail</TableCell>
              <TableCell align="right">Ammount</TableCell>
              <TableCell align="right">budget Limit</TableCell>
              <TableCell align="right">Currency</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {wallets.map((wallet) => (
              <TableRow key={wallet.wallet_id}>
                <TableCell component="th" scope="row">
                  {wallet.name}
                </TableCell>
                <TableCell align="right">{wallet.clientEmail}</TableCell>
                <TableCell align="right">{wallet.amount}</TableCell>
                <TableCell align="right">{wallet.budgetLimit}</TableCell>
                <TableCell align="right">{wallet.currency}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

        <div class="block">
          <div class="currency">Choose currency</div> 
          <Select
          onChange={(e) => handleCurrencyChange(e.target.value)}
          >
          <MenuItem value="CZK">CZK</MenuItem>
          <MenuItem value="USD">USD</MenuItem>
          <MenuItem value="EUR">EUR</MenuItem>
          </Select>
        </div>

        <div style={{ display: 'flex', alignItems: 'center' }}>
          <TextField
            type="number"
            label="Введите число"
            variant="outlined"
            value={inputValue}
            onChange={handleInputChange}
            style={{ marginRight: '10px' }}
          />
          <Button
            variant="contained"
            color="primary"
            onClick={handleAddMoney}
          >
            Add money to wallet
          </Button>
        </div>
      </div>
  );
};

export default WalletsListPage;
