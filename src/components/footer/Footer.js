import React from "react";
import "./Footer.css";

const Footer = () => {
  return (
    <div className="footer">
      <div className="contact-info">
        <span className="icon">📞</span>
        <span>+420 123 456 789</span>
        <span>Klientská linka (denně 7-22 hod)</span>
      </div>
      <div className="email-info">
        <span className="icon">✉</span>
        <span>info@pm.cvut.fel.cz</span>
        <span>Odpovíme do 24 hodin</span>
      </div>
    </div>
  );
};

export default Footer;
