import React from "react";
import "./Header.css";

const Header = () => {
  const username = localStorage.getItem('username') || 'Неизвестный пользователь';
  const email = localStorage.getItem('email') || 'email@example.com';



  return (
    <div className="header">
      <div className="header-icons">
        <a href="/registration"><span style={{ cursor: 'pointer', marginRight: "10px" }}>Log out</span></a>
        <span className="icon">✉</span>
        <span className="icon">📁</span>
        <span className="icon">⚙</span>
      </div>
      <div className="header-user-info">
        <span>{username}</span>
        <span>{email}</span>
      </div>
    </div>
  );
};

export default Header;
