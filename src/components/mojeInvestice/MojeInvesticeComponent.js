import React from "react";
import { NavLink } from "react-router-dom";
import "./MojeInvesticeComponent.css";

const MojeCartyComponent = () => {
  return (
    <div class="card">
      <img src="./karta.png" />
      <div>Cena: 19.27$</div>
      <div>Vaše investice: 120$</div>
    </div>
  );
};

export default MojeCartyComponent;
