import React from "react";
import { NavLink } from "react-router-dom";
import "./MojeCartyComponent.css";

const MojeCartyComponent = () => {
  return (
    <div class="card">
      <img src="./karta.png" />
      <div>Platnost: 11/28</div>
      <div>Number: **** **** **** 1879</div>
    </div>
  );
};

export default MojeCartyComponent;
