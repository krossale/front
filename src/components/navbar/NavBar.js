import React from "react";
import { NavLink } from "react-router-dom";
import "./NavBar.css";

const NavBar = () => {
  return (
    <nav>
      <ul>
        <li>
          <NavLink to="/new-payment" activeClassName="active">
            Новая платёж
          </NavLink>
        </li>
        <li>
          <NavLink to="/payment-overview" activeClassName="active">
            Платёж
          </NavLink>
        </li>
        <li>
          <NavLink to="/transfer" activeClassName="active">
            Перевод межи властными учёты
          </NavLink>
        </li>
        <li>
          <NavLink to="/currency-conversion" activeClassName="active">
            Конверзе мен
          </NavLink>
        </li>
        <li>
          <NavLink to="/taxes" activeClassName="active">
            Taxes
          </NavLink>
        </li>
        <li>
          <NavLink to="/payment-history" activeClassName="active">
            Преходы платеб
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default NavBar;
