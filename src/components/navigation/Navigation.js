import React from "react";
import { NavLink } from "react-router-dom";
import "./Navigation.css";

const Navigation = () => {
  return (
    <div className="navigation">
      <NavLink to="/categories" activeClassName="active">
        Categories
      </NavLink>
      <NavLink to="/wallet" activeClassName="active">
        My Wallets
      </NavLink>
      <NavLink to="/transactions" activeClassName="active">
        Transactions
      </NavLink>
    </div>
  );
};

export default Navigation;
