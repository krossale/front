import React from "react";
import { NavLink } from "react-router-dom";
import "./RightBar.css";

const RightBar = () => {
  return (
    <div class="right-bar">
      <div class="up">
        <div class="up-header">
          <div>Čekající položky</div>
        </div>
        <div class="up-body">
          <div>Nemáte žádné čekající položky.</div>
        </div>
      </div>

      <div class="down">
        <div class="down-header">
          <div>Čekající položky</div>
        </div>
        <div class="down-body">
          <div>Nabídky </div>
          <div>Účty a spoření </div>
          <div>Investice </div>
          <div>Přepnout účty </div>
          <div>Tipy pro Vás </div>
        </div>
      </div>
    </div>
  );
};

export default RightBar;
