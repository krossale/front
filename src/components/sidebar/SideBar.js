import React, { useState } from "react";
import "./Sidebar.css";

const SidebarItem = ({ title, onSelect }) => (
  <li className="sidebar-item" onClick={onSelect}>
    <a>{title}</a>
  </li>
);

const Sidebar = ({ items }) => {
  const [activeComponent, setActiveComponent] = useState(null);

  return (
    <div class="page">
      <div className="sidebar">
        <ul className="sidebar-list">
          {items.map((item, index) => (
            <SidebarItem
              key={index}
              title={item.title}
              onSelect={() => setActiveComponent(item.component)}
            />
          ))}
        </ul>
      </div>
      <div className="content">{activeComponent}</div>
    </div>
  );
};

export default Sidebar;
